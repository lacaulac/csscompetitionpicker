<?php
$db = new SQLite3('database.sqlite'); //Opens database
if($_POST['id'] && $_POST['func'] == "join")
{
  $answer = $db->query("SELECT * from 'lobbys' where id=" . $_GET['id'] . ";"); //Gets the lobby by id
  $lobby = $answer->fetchArray(); //Gets lobby in an array
  $players = json_decode($lobby['Players']); //Gets an array of the list of players
  $playersnb = count($players); //Counts the quantity of players in the lobby
  if(getMaxPlayers($lobby['Gamemode']) > $playersnb) //Is the lobby full already?
  {
    $players[] = $_SESSION['username']; //Adds the player into the array
    $players = json_encode($players); //Encode the array in JSON, ready for getting in DB
    $db->query("UPDATE 'lobbys' SET Players='" . $players . "' WHERE id=" . $_GET['id'] . ";"); //Put the modified list in the DB
    $_SESSION['lobbyid'] = $_GET['id']; //Sets the session as "In a lobby"
    echo "joined";
  }
  else
  {
    echo "failed";
  }
}
if($_POST['func'] == "quit")
{
  $answer = $db->query("SELECT * from 'lobbys' where id=" . $_SESSION['lobbyid'] . ";");
  $answer = $answer->fetchArray();
  $persons = $answer['Players'];
  $persons = json_decode($persons);
  for($i=0; $i<count($persons); $i++)
  {
    if($persons[i] == $_SESSION['username'])
    {
      array_splice($persons, i);
      break;
    }
  }
  $persons = json_encode($persons);
  $db->exec("UPDATE 'lobbys' SET Players='" . $persons . "' WHERE id=" . $_SESSION['lobbyid'] . ";");
  $_SESSION['lobbyid'] = "none";
}
?>
