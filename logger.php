<htmL>
<head>
	<title>CSS Gather login page</title>
<meta charset="utf-8"/>
<link rel="stylesheet" href="main.css"/>
</head>
<body>
<?php
session_start();
require ('steamauth/steamauth.php');
if(isset($_POST['username']))
{
$_SESSION['username'] = $_POST["username"];
$_SESSION['lobbyid'] = "none";
$redir = true;
}
if($_POST["username"] == "disconnect")
{
	session_destroy();
	$redir = true;
}
if($redir)
{
?>
<script>
setTimeout(function(){
	document.location = "index.php";
}, 3000)
</script>
You're gonna be redirected on the main page in a few seconds.
<?php
}
else
{
	?>
	<h1>Authentification</h1>
	<content>
		<?php
		if(!isset($_SESSION['steamid']))
		{
				echo "<h3>Connection with Steam:</h3>";
		    steamlogin(); //login button
		}
		else
		{
			include('steamauth/userInfo.php');
			include("functions.php");
			if(isVACBanned($steamprofile['steamid']))
		  {
		    session_destroy();
				?>
				<script>
				alert("This account has been VAC banned. So you can't use this website.");
				</script>
				<?php
		  }
		  else
		  {
				$_SESSION['username'] = htmlspecialchars($steamprofile['personaname'], ENT_QUOTES);
				$_SESSION['lobbyid'] = "none";
		  }
			?>
			<script>
			setTimeout(function(){
				document.location = "index.php";
			}, 3000)
			</script>
You're gonna be redirected on the main page in a few seconds.
<?php
		}
		?>
	</content>
	<?php
}
?>
<footer>
	Created by lacaulac. Work under Creative Commons BY-NC. <a href="https://bitbucket.org/lacaulac/csscompetitionpicker">Source code here</a>
</footer>
</body>
</html>
