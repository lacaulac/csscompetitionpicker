-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 04 Mai 2015 à 07:05
-- Version du serveur: 5.5.9
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `matchpicker`
--

-- --------------------------------------------------------

--
-- Structure de la table `lobbys`
--

CREATE TABLE `lobbys` (
  `ID` int(100) NOT NULL,
  `Creator` varchar(100) CHARACTER SET utf8 NOT NULL,
  `ServerCmd` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Players` varchar(100) CHARACTER SET utf8 NOT NULL,
  `Gamemode` varchar(100) NOT NULL,
  `Map` varchar(100) NOT NULL,
  UNIQUE KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `lobbys`
--

INSERT INTO `lobbys` VALUES(0, 'lacaulac', 'sv_cheats 1', '["lacaulac","darkeo","splittt"]', '3v3', 'de_dust2');
