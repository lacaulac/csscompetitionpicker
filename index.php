<html>
<head>
<title>Competitive match picker</title>
<link rel="stylesheet" href="main.css"/>
<script src="main.js"></script>
<meta charset="utf-8"/>
</head>
<body>
<header><h1>CS:S MatchPicker</h1></header>
<content>
<?php
	session_start();
	include ('steamauth/steamauth.php');
	include("config.php");
	include("functions.php");
	checkUser();
	include("welcome.php");
	include("lobbyhandle.php");
?>
</content>
<footer>
	Created by lacaulac. Work under Creative Commons BY-NC. <a href="https://bitbucket.org/lacaulac/csscompetitionpicker">Source code here</a>
</footer>
</body>
</html>
